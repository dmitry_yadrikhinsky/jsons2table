import * as fs from 'fs';
import * as path from 'path';
import { ExplorerJson } from "./explorerJson";
import { table } from 'table';

export class Explorer {
  private explorers: ExplorerJson[];

  constructor(public pathWithJson: string) {
    const files = fs.readdirSync(pathWithJson);
    this.explorers = files
      .map(file => path.join(pathWithJson, file))
      .map(json => new ExplorerJson(json))
      .filter(e => e.json != null);
  }

  listPath(paths: string[]) {
    const output: string[][] = [];
    const header = paths.slice();

    this.explorers.forEach(json => {
      const line = this.getValues(json, paths);
      output.push(line);
    });

    output.unshift(header);
    const outTable = table(output);
    console.log(outTable);
  }

  private getValues(json: ExplorerJson, paths: string[]) {
    return paths.map(path => {
      if (path === '%filename') {
        return json.jsonName;
      }
      return json.getValue(path);
    });
  }
}

