import { Command } from 'commander';
import {Explorer} from "./explorer";

let inputPath: string;
let jsonPath: string[];

new Command()
  .version('0.0.1')
  .option(`-i, --input`, `Path with json's`)
  .option(`-k, --keys`, `What keys to show`)
  .action((input, keys) => {
    inputPath = input;
    jsonPath = keys.split(',');
  })
  .parse(process.argv);

new Explorer(inputPath!)
  .listPath(jsonPath!);
