import * as fs from 'fs';
import * as path from 'path';

export class ExplorerJson {
  readonly json: any;
  readonly jsonName: string;

  constructor(public jsonPath: string) {
    this.jsonName = path.basename(jsonPath);
    this.json = this.parseJson(jsonPath);
  }

  getValue(key: string) {
    return this.json[key];
  }

  private parseJson(path: string) {
    try {
      const buffer = fs.readFileSync(path, 'utf8');
      return JSON.parse(buffer);
    } catch (e) {
      console.error(`Can't read file: "${path}"`);
    }
  }
}
