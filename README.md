### About
This tool allows you to view json in a convenient way

### Before start to use

    npm i

### Example
- Download folder with jsons from s3
        
        aws s3 sync s3://valantmed.virgo.dev/pa/envs/n7/libraries/customer/exportable/AwesomeCustomer/WorkingVersion/forms C:\path_to_jsons

- Convert to table
        
        npm start -- --input C:\path_to_jsons --keys %filename,key,version


#### Input:

cd C:\path_to_jsons

```
{ key: 'EncounterDetailsA', version: '1.0' ... } // 00d752d1-3a43-4f9b-b6f0-a25a5c7e6ab2
{ key: 'EncounterDetailsB', version: '1.1' ... } // 02ce8f5b-524c-4661-93b9-74ed29d10fc5
{ key: 'EncounterDetailsB', version: '1.20'... } // 035a86ff-dac7-48d6-a5b2-275637f9e4a0
```

#### Output:
```
╔══════════════════════════════════════╤═════════════════════════╤═════════╗
║ %filename                            │ key                     │ version ║
╟──────────────────────────────────────┼─────────────────────────┼─────────╢
║ 00d752d1-3a43-4f9b-b6f0-a25a5c7e6ab2 │ EncounterDetailsA       │ 1.0     ║
╟──────────────────────────────────────┼─────────────────────────┼─────────╢
║ 02ce8f5b-524c-4661-93b9-74ed29d10fc5 │ EncounterDetailsB       │ 1.1     ║
╟──────────────────────────────────────┼─────────────────────────┼─────────╢
║ 035a86ff-dac7-48d6-a5b2-275637f9e4a0 │ Select                  │ 1.20    ║
╟──────────────────────────────────────┼─────────────────────────┼─────────╢
```
[[Table render](https://github.com/gajus/table)]
